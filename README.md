## Impulse Shopping Box

The one stop shop for subscription boxes with new, surprise items! Different every month, sent on our impulse!

Users can add a box of their choosing to their shopping carts, and then complete a checkout process. They can also login - with authentication! And, there is the option to browse possible box items for either box type.

--

This site was a collaboration among the 4 of us: Morgan, Adrian, Evan and Nellie, the MEAN Coders. We parceled out the work at regular standups, and while we each started with responsibility for specific features, we'd often collaborate or overlap.

The main features I worked with were the Clothing back-end, the signup form, the Products data, and the About Us page. I also learned about things like not adding djwto tokens where they're not needed; how CRLF (line-ending) differences in operating systems can be time-consuming to diagnose, though quick to fix; and how to implement favicons for our brand instead of React's default one.

To-dos:
- New photos needed for all Products cards. Originally the photos were sourced from third-party data, now no longer available. Platzi Fake Store, we'll miss you.
- Finish deployment to Heroku.

## Intended Market

Anyone who is curious about trying new things, or who likes to be surprised.

## Functionality

Users:

- Users will create an account to log in and be able to log out as well.
- Users will choose between a "Things" box or a "Styles" box, and a certain number of items will be in the box.

Inventory:

- Inventory will update with past and possible items for boxes

## Design

[API design](docs/apis.md)<br>
[Data model](docs/data-model.md)<br>
[GHI](docs/ghi.md)<br>
[Integrations](docs/integrations.md)

## Instructions to run Impulse locally

Open Docker Desktop, then:

For Mac users:

- Fork, Clone, and Star the repository from [Shopping Box](https://gitlab.com/WhatABoolBelieves/shopping-box)
- In terminal, run bash hard-reset.sh
  For soft resets:
- run bash soft-reset.sh

(dev note: hard-reset.sh MUST be run on first start or no databases will exist)

For Windows users:

- Fork, Clone, and Star the repository from [Shopping Box](https://gitlab.com/WhatABoolBelieves/shopping-box)
- On first start, in powershell, run:
  _ docker volume create postgres-data
  _ docker volume create pgadmin \* docker-compose up --build
  For soft resets, in powershell:
- docker-compose down
- docker-compose up --build

(dev note: the hard-reset.sh and soft-reset.sh files can be ran on Windows with a git bash terminal or WSL)

Congratulations, you've now installed Impulse on your local machine!


Once the containers are running:

- Enter the Docker CLI for products
- Enter: `python manage.py loaddata inventory.json`
- Things and Styles pages should now be populated with product cards!

- Subscription boxes are created in house and based on a monthly item selection from the product pool, so those are the only things you can buy.

- Add one to your cart and do the checkout form. This should take you to an adorable receipt page!

## Unit Tests

Unit Tests exist for Impulse and can be enabled by following these steps:

- navigate to the pgadmin container here: [PgAdmin](http://localhost:8060)
- Login with the supplied username and password on the docker-compose.yaml file under the 'pgadmin' service.
- Once logged in, register the server by right clicking server then Register > Server.
- In the window pop-up, provide a name for the database
- Navigate to the connections tab on top of the pop-up window, Host name/address will be 'postgres'
- Username will be postgres (as we need to be able to provide admin permission)
- Password is supplied in the docker-compose.yaml under the 'postgres' service
- Save the password for ease of access and save the form, the pop-up should disappear.
- Once the server is registered, click the dropdown on servers, then the dropdown for Login/Group Roles
- Right-click products, click properties in the pop-up, navigate to 'Privileges' in the pop-up
- Make sure create databases is toggled on and save.
- Repeat the same steps for subscriptions
- This allows for test-databases to be created in the project


To run tests:

Set up pgadmin:
- go to localhost:8060/login in the browser. Log in with any email and password.
- Click on Add New Server; name it `postgres-data`
- Click on Connection tab. For Host name and Username: `postgres`; password: `test-databases`. Save.
- On the right side, click Servers >> Login/Group Roles
  - Right-click on products >> Properties. Click on Privileges. On Create databases? slide the slider right.
  - Right-click on subscriptions >> Properties. Click on Privileges. On Create databases? slide the slider right.

Then:
- Enter the Docker CLI or use the bash command to enter the container from terminal
- Enter: `python manage.py test`
- This should run the tests for the individual service, repeat the steps in the other service to test that one.
