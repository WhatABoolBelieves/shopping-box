
stages:
  - test
  - build
  - deploy

services:
  - postgres:14.2-bullseye

variables:
  POSTGRES_PASSWORD: test
  DATABASE_URL: postgresql://postgres:test@postgres:5432/postgres
  REACT_APP_SUBSCRIPTIONS: https://${HEROKU_ACCOUNT_SUBSCRIPTIONS}.herokuapp.com
  REACT_APP_PRODUCTS: https://${HEROKU_ACCOUNT_PRODUCTS}.herokuapp.com
  REACT_APP_SUBSCRIPTIONS_POLLER: https://${HEROKU_ACCOUNT_SUBS_POLLER}.herokuapp.com

subscriptions-unit-test-job:
  image: python:3-bullseye
  stage: test
  script:
    - cd ./subscriptions/api
    - pip install --upgrade pip
    - pip install -r requirements.txt
    # - export one of the urls? database one? see 1st page CI ckbk
    # - python manage.py migrate
    - python manage.py test

products-unit-test-job:
  image: python:3-bullseye
  stage: test
  script:
    - cd ./products
    - pip install --upgrade pip
    - pip install -r requirements.txt
   # - export one of the urls? see 1st page CI ckbk
   # - python manage.py migrate
    - python manage.py test

lint-job:
  image: python:3-bullseye
  stage: test
  script:
    - pip install flake8
    - flake8 products subscriptions

# BUILD ----------------------------------------------------

build-products-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  script:
    - cd products
    - docker build -t ${CI_REGISTRY_IMAGE}/products:latest .
    - docker tag ${CI_REGISTRY_IMAGE}/products:latest ${CI_REGISTRY_IMAGE}/products:$CI_JOB_ID
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push ${CI_REGISTRY_IMAGE}/products:$CI_JOB_ID
    - docker push ${CI_REGISTRY_IMAGE}/products:latest

build-subscriptions-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  script:
    - cd subscriptions/api
    - docker build -t ${CI_REGISTRY_IMAGE}/subscriptions/api:latest .
    - docker tag ${CI_REGISTRY_IMAGE}/subscriptions/api:latest ${CI_REGISTRY_IMAGE}/subscriptions/api:$CI_JOB_ID
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push ${CI_REGISTRY_IMAGE}/subscriptions/api:$CI_JOB_ID
    - docker push ${CI_REGISTRY_IMAGE}/subscriptions/api:latest

build-subscriptions-poller-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  script:
    - cd subscriptions/poll
    - docker build -t ${CI_REGISTRY_IMAGE}/subscriptions/poll:latest .
    - docker tag ${CI_REGISTRY_IMAGE}/subscriptions/poll:latest ${CI_REGISTRY_IMAGE}/subscriptions/poll:$CI_JOB_ID
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push ${CI_REGISTRY_IMAGE}/subscriptions/poll:$CI_JOB_ID
    - docker push ${CI_REGISTRY_IMAGE}/subscriptions/poll:latest

build-front-end-job:
  variables:
    PUBLIC_URL: https://whataboolbelieves.gitlab.io/shopping-box
  stage: build
  image: node:lts-bullseye
  script:
    - cd ghi          # because this is where package.json file is
    - npm install
    - npm run build
    - cp build/index.html build/404.html        # Make this a SPA
  artifacts:
    paths:
      - ghi/build/

# DEPLOY ----------------------------------------------------

deploy-products-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: deploy
  image: docker:20.10.16
  needs:
    - build-products-image-job
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker login --username=_ --password=$HEROKU_API_KEY registry.heroku.com
    - docker pull ${CI_REGISTRY_IMAGE}/products:latest
    - docker tag ${CI_REGISTRY_IMAGE}/products:latest registry.heroku.com/$HEROKU_ACCOUNT_PRODUCTS/web:latest
    - docker push registry.heroku.com/$HEROKU_ACCOUNT_PRODUCTS/web:latest

deploy-subscriptions-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: deploy
  image: docker:20.10.16
  needs:
    - build-subscriptions-image-job
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker login --username=_ --password=$HEROKU_API_KEY registry.heroku.com
    - docker pull ${CI_REGISTRY_IMAGE}/subscriptions/api:latest
    - docker tag ${CI_REGISTRY_IMAGE}/subscriptions/api:latest registry.heroku.com/$HEROKU_ACCOUNT_SUBSCRIPTIONS/web:latest
    - docker push registry.heroku.com/$HEROKU_ACCOUNT_SUBSCRIPTIONS/web:latest

deploy-subscriptions-poller-image-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  stage: deploy
  image: docker:20.10.16
  needs:
    - build-subscriptions-poller-image-job
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker login --username=_ --password=$HEROKU_API_KEY registry.heroku.com
    - docker pull ${CI_REGISTRY_IMAGE}/subscriptions/poll:latest
    - docker tag ${CI_REGISTRY_IMAGE}/subscriptions/poll:latest registry.heroku.com/$HEROKU_ACCOUNT_SUBS_POLLER/worker:latest
    - docker push registry.heroku.com/$HEROKU_ACCOUNT_SUBS_POLLER/worker:latest

pages:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  dependencies:
    - build-front-end-job
  needs:
    - build-front-end-job
  script:
    - cd ghi
    - cp build/index.html build/404.html
    - mv ghi/build/ public
  artifacts:
    paths:
      - public

release-products-image-job:
  stage: deploy
  image: node:10.17-alpine
  needs:
    - deploy-products-image-job
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - apk add curl bash
    - curl https://cli-assets.heroku.com/install.sh | sh
  script:
    - heroku container:release web --app $HEROKU_ACCOUNT_PRODUCTS

release-subscriptions-image-job:
  stage: deploy
  image: node:10.17-alpine
  needs:
    - deploy-subscriptions-image-job
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - apk add curl bash
    - curl https://cli-assets.heroku.com/install.sh | sh
  script:
    - heroku container:release web --app $HEROKU_ACCOUNT_SUBSCRIPTIONS

release-subscriptions-poller-image-job:
  stage: deploy
  image: node:10.17-alpine
  needs:
    - deploy-subscriptions-poller-image-job
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - apk add curl bash
    - curl https://cli-assets.heroku.com/install.sh | sh
  script:
    - heroku container:release worker --app $HEROKU_ACCOUNT_SUBS_POLLER

